# Pferderennen

Das hier ist ein Nachbau vom Maus-Skateboard-Spiel, weil mein Kind das gern mit Pferden hätte. :)

## Tech stack

- HTML5
- CSS
- Vanilla JS (ES6)
- irgendein Webserver. Ich benutz gern das python-modul um's lokal laufen zu lassen (siehe [Makefile](Makefile))

## Entwickeln

`nix-shell` starten und alles ist gut :)

