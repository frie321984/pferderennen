import Game from './js/Game.js';
import Startknopf from './js/startknopf.js';
import DebugSettings from './js/debugSettings.js';
import Settings from './js/settings.js';

var game = new Game();
var startknopf = new Startknopf(game.canvas);
var settings = new Settings();
var debugOptions = new DebugSettings();

game.render();
startknopf.render();
settings.render();
debugOptions.render();

/*
TODOs
-----------
* Touchsteuerung hakelt etwas / manchmal erkennt er pointerup nicht und läuft dann ewig lang
* Stern oben rechts wird nicht angezeigt
* Namen von Spieler:in und Pferd eingeben
* Highscore im Browsercache
* svgs schrumpfen
* Anderes Bild fürs hinfallende Pferd + Geräusch
* Pferd customizen: Fell- Haar- Augenfarbe
* große Hindernisse überhüpfen gibt 3 Punkte
* bewegende Hindernisse
* Schwierigkeitsgrad leicht/schwer mit unterschiedl. Geschwindigkeiten
* 2-Player Modus 
* Startknopf ins Canvas / ENTER drücken
* "Weg" in den Hintergrund packen
* Hintergrundmusik reagiert nicht auf isMuted

Erledigt
---------------
+ Sterne ausmalen
+ Maus/Touchsteuerung
+ Pferdebild auf dem Start / Endbildschirm
+ Punkte anzeigen während des Spiels
+ mehrere Level
+ Verhindern dass das Pferd links oder rechts ausbüchsen kann
+ Hüpfbild verbessern
+ Hüpfton einbauen
+ Hintergrundmusik
+ Geräusch beim Hüpfen
+ Hufgetrappel fehlt noch immer wenn das pferd läuft, außer beim hüpfen
*/
