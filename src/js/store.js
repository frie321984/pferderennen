import Store from './beedle.js';
import latestEvent from './latestEvent.js';

const actions = {
    saySomething(context, payload) {
        context.commit('setMessage', payload);
    },
	start(context) {
		context.commit('start');
	},
	alleLebenVerloren(context) {
		this.ende(context)
	},
	ende(context) {
		context.commit('ende');
	},
	levelStart(context, payload) {
		context.commit('levelStart', payload)
	},
	levelEnde(context, payload) {
		context.commit('levelEnde', payload)
	},
	sternGesammelt(context) {
		context.commit('sternGesammelt');
	},
	setDebugMode(context, payload) {
		context.commit('setDebugMode', payload)
	},
	setMute(context, payload) {
		context.commit('setMute', payload);
	},
	hindernisGetroffen(context) {
		let autsch = 1 + context.state.events.filter(e => e === 'AUTSCH').length
		let leben = Math.max(context.state.anzahlLeben - autsch, 0)

		context.commit('hindernisGetroffen', { uebrigeLeben: leben})
		context.commit('schmerzVerarbeiten')
		if (leben == 0) {
			context.commit('ende', { verloren: true })
		}
	},
};
const mutations = {
    setMessage(state, payload) {
        state.message = payload;

        return state;
    },
	start(state) {
		state.verloren = false;
		state.uebrigeLeben = state.anzahlLeben
		state.heldin = initialState.heldin;
		state.timeRemainingMs = initialState.timeRemainingMs;
		state.isStarted=true;
		state.events = ['PRESTART'];
		state.events.push('START');
		state.count=0;

		return state;
	},
	levelStart(state, payload) {
		state.events.push('LEVEL-START')
		state.count++;
		return state;
	},
	levelEnde(state, payload) {
		state.events.push('LEVEL-END')
		state.count++;
		return state;
	},
	ende(state, payload) {
		if (latestEvent(state) === 'ENDE') {
			return;
		}
		if (payload && payload.verloren) {
			state.verloren = payload.verloren
		}
		state.events.push('PREENDE');
		state.count++
		state.isStarted = false;
		state.events.push('ENDE');
		state.count++
		return state;
	},
	sternGesammelt(state) {
		state.events.push('STERN');
		state.count++
		return state;
	},
	setDebugMode(state, payload) {
		state.isDebugging = payload.isOn;
		state.gameDurationMs = payload.gameDurationMs || init.gameDurationMs
		state.anzahlLeben = payload.anzahlLeben || init.anzahlLeben
		return state;
	},
	setMute(state, payload) {
		state.isMuted = payload.isOn;
		return state
	},
	schmerzVerarbeiten(state, payload) {
		state.events.push('SCHMERZEN')
		state.count++
		return state;
	},
	hindernisGetroffen(state, payload) {
		state.events.push('AUTSCH')
		state.uebrigeLeben = payload.uebrigeLeben
		state.count++
		return state
	},
};
const initialState = {
	isStarted: false,
	heldin: {x:0, y:0},
	gameDurationMs: 1000 * 60 * .8, // minuten
	isMuted: false,
	isDebugging: false,
	schmerzDauerMs: 5000,
	anzahlLeben: 3,
	uebrigeLeben: 0,
	verloren: false,
	count:0
};

export default new Store({
    actions,
    mutations,
    initialState
});

export const init = {...initialState}