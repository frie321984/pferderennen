import latestEvent from "./latestEvent.js";
import store from "./store.js";
import MyAudio from "./MyAudio.js";

export default class Hintergrundmusik {
    constructor() {
		this.self = this;
		
		this.SND_MUSIK = new MyAudio("sound/Pferdehuntergrundmusik.mp3");
		this.SND_MUSIK.lowVolume()

		store.subscribe((state) => {
			if (latestEvent(state) == "PRESTART") {
				this.SND_MUSIK.playInLoop()
			} else if (latestEvent(state) == "PREENDE") {
				this.SND_MUSIK.pause()
			}
		})
	}
}