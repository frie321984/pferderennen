import store from './store.js';
import Heldin from './heldin.js';
import MyImage from './MyImage.js';
import Endanzeige from './endanzeige.js';
import Anleitung from './anleitung.js';
import latestEvent from './latestEvent.js';
import Gesundheitsanzeige from './Gesundheitsanzeige.js'
import DebugHud from './DebugHud.js';
import DefaultLevel from './DefaultLevel.js';
import SpringDings from './hindernisse/Springdings.js'
import Helm from './hindernisse/Helm.js'
import Reitgerte from './hindernisse/Reitgerte.js'
import Punktetafel from './Punktetafel.js';


function resize(g) {
    var canvs = g.canvas, width = window.innerWidth, height = window.innerHeight;
    var wratio = width / height, ratio = canvs.width / canvs.height;
    if (wratio < ratio) {
        canvs.style.width = width + "px";
        canvs.style.height = (width / ratio) + "px";
    } else {
        canvs.style.width = (height * ratio) + "px";
        canvs.style.height = height + "px";
    }
}

export default class Game {
    constructor() {
		this.canvas = document.createElement("canvas");
		this.canvas.id = 'canvas'
		this.canvas.width=1000
		this.canvas.height=480
		document.getElementById("spiel").appendChild(this.canvas);
		this.self = this;

		this.heldin = new Heldin(this.canvas.width);
		this.levels = [
			new DefaultLevel("Level 1", this.canvas, this.heldin)
				.withHindernisProvider(() => { return new Helm()}),
			new DefaultLevel("Level 2", this.canvas, this.heldin)
				.withHindernisProvider(() => {
					let r2 = Math.round(Math.random()*2);
					switch(r2) {
						case 1: return new Reitgerte();
						case 0: // fallthrough
						default: return new Helm();
					}
				}),
			new DefaultLevel("Level 3", this.canvas, this.heldin)
				.withHindernisProvider(() => {
					let r2 = Math.round(Math.random()*3);
					switch(r2) {
						case 2: return new SpringDings();
						case 1: return new Reitgerte();
						case 0: // fallthrough
						default: return new Helm();
					}
				}),			
		]
		this.currentLevel = 0;

		this.endAnzeige = new Endanzeige();
		this.anleitung = new Anleitung();
		this.health = new Gesundheitsanzeige();
		this.debugHud = new DebugHud();
		this.punktetafel = new Punktetafel();

		this.bg = new MyImage("images/wiese.svg", () => { this.render() }).withWidth(this.canvas.width);
		this.isRunning = false;
		
		store.subscribe((state) => {
			if (!this.isRunning && latestEvent(state) === 'START') {
				console.log('Let the games begin!')
				this.duration = state.gameDurationMs;
				this.startGame(state.gameDurationMs)
			}
			if (this.isRunning && latestEvent(state) === 'ENDE') {
				this.isRunning=false
			}
			if (this.isRunning && latestEvent(state) === 'LEVEL-END') {
				this.currentLevel++;
				this.then = Date.now();
				this.endTime = new Date(Date.now() + state.gameDurationMs)
				this.remainingTime = this.endTime-this.then;
				store.dispatch('levelStart', {levelNumber: this.currentLevel, startTime: this.then})
			}
		})

		window.addEventListener('resize', () => resize(this));
		resize(this);
		
    }
	
	startGame(duration) {
		this.isRunning = true;
		this.then = Date.now();
		this.endTime = new Date(Date.now() + duration)
		this.remainingTime = this.endTime-this.then;

		this.currentLevel = 0;
		store.dispatch('levelStart', {levelNumber: this.currentLevel, startTime: this.then})
		this.mainGameLoop()
	}

	mainGameLoop() {
		// Cross-browser support for requestAnimationFrame
		let w = window;
		requestAnimationFrame = w.requestAnimationFrame || w.webkitRequestAnimationFrame || w.msRequestAnimationFrame || w.mozRequestAnimationFrame;

		let now = Date.now();

		this.tick(now)
		this.render()

		this.then = now;
		
		if (this.isRunning) {
			// Request to do this again ASAP
			requestAnimationFrame(() => { this.mainGameLoop() });
		} else {
			requestAnimationFrame(() => { this.render() });
		}
	}

	tick(now) {

		let delta = now - this.then;
		this.remainingTime = this.endTime - now;

		if (this.remainingTime <= 0) {
			if (this.levels[this.currentLevel+1]) {
				store.dispatch('levelEnde', {levelNumber: this.currentLevel})
			} else {
				store.dispatch('ende')
			}
		}

		this.levels[this.currentLevel].tick(delta, this.remainingTime)
	}

    render() {
		// Background Layer
		this.bg.render(this.canvas);

		// sternlayer
		if (this.isRunning) {
			this.levels[this.currentLevel].render()

			// top layer - score
			this.health.render(this.canvas)

			this.debugHud.render(this.canvas, this.remainingTime)

			this.punktetafel.render(this.canvas);
		}
		this.endAnzeige.render(this.canvas)
		this.anleitung.render(this.canvas)
    }
}