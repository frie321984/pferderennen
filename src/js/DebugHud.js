import store from "./store.js";

export default class DebugHud {
	constructor() {
		this.self = this;
		this.remainingTimeMs = '';
		this.isVisible=false;
		store.subscribe((state) => {
			if (!state) {
				return
			}
			this.isVisible = state.isDebugging && state.isStarted;
		})
    }

    render(canvas, remainingTimeMs='?') {
		if (!this.isVisible) {
			return
		}
		let context = canvas.getContext("2d")

		context.fillStyle = "black";
		context.font = "20px Arial";
		context.textAlign = "left";
		context.textBaseline = "top";
		context.fillText(remainingTimeMs, 10 , 30);
	}
}