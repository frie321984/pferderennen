import store from "./store.js";
import latestEvent from "./latestEvent.js";
export default class MyAudio {
    constructor(filename="sound/Yay.mp3", onlyPlayWhileGameInProgress=true) {	
		let a = new Audio(filename);
		this.isMuted = true;
		this.audio = a;

		this.isMuted = store.state.isMuted;
		store.subscribe((state) => {
			this.isMuted = state.isMuted;
		})
		if (onlyPlayWhileGameInProgress) {
			store.subscribe((state) => {
				if (latestEvent(state) == "PREENDE") {
					this.audio.pause()
				}
			})
		}
    }

	lowVolume() {
		this.audio.volume = 0.35
	}

	play() {
		if (this.isMuted) {
			return
		}
		this.audio.play()
	}

	playInLoop() {
		this.audio.loop = true
		this.audio.play()
	}

	pause() {
		this.audio.loop = false
		this.audio.pause()
	}
}