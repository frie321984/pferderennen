import {default as store} from "./store.js";

export default class Settings {
    constructor() {
		this.self = this;

		let toggle = document.createElement("input")
		toggle.type = "checkbox"
		toggle.id="pferderennen.muteToggle"
		let lbl = document.createElement("label")
		lbl.htmlFor = "pferderennen.muteToggle"
		lbl.innerText="🔇🔊"

		let div = document.createElement("div");
		div.id="pferderennen.settings"
		div.appendChild(toggle)
		div.appendChild(lbl)
		document.getElementById("spiel").appendChild(div)

		this.toggle = toggle
		this.lbl = lbl

		this.isMuted = store.state.isMuted;
		this.isSoundOn = !store.state.isMuted
		toggle.addEventListener("click", event => {
			store.dispatch('setMute', {isOn: !event.target.checked})
		})
		store.subscribe((state) => {
			this.isMuted = state.isMuted;
			this.isSoundOn = !store.state.isMuted
			this.render()
		});
	}

	render() {
		this.toggle.checked = this.isSoundOn
		this.lbl.innerText= this.isSoundOn ? "🔊 Ton ist an": "🔇 Ton ist aus"
	}

}