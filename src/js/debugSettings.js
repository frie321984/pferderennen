import {default as store, init} from "./store.js";

export default class DebugSettings {
    constructor() {
		this.self = this;

		let url = new URL(window.location.href);
		let searchParams = new URLSearchParams(url.search);
		this.disabled = false;
		this.isVisible=searchParams.has('dbg')

		let debugToggle = document.createElement("input")
		debugToggle.type = "checkbox"
		debugToggle.id="pferderennen.debugToggle"
		let lbl = document.createElement("label")
		lbl.htmlFor = "pferderennen.debugToggle"
		lbl.innerText="debug mode"

		debugToggle.addEventListener("click", event => {
			store.dispatch('setDebugMode', {
				isOn: event.target.checked,
				gameDurationMs: event.target.checked? 8000:init.gameDurationMs,
				anzahlLeben: event.target.checked ? 1: init.anzahlLeben
			})
		})

		let dbgForm = document.createElement("form");
		dbgForm.id="pferderennen.debug-options"
		dbgForm.appendChild(debugToggle)
		dbgForm.appendChild(lbl)
		document.getElementById("spiel").appendChild(dbgForm)

		this.isDebugging = store.state.isDebugging;
		this.isVisible = searchParams.has('dbg') && !store.state.isStarted
		
		this.debugToggle = debugToggle
		this.element = dbgForm;

		store.subscribe((state) => {
			this.isDebugging = state.isDebugging;
			this.isVisible = searchParams.has('dbg') && !store.state.isStarted
			this.render()
		});

		store.subscribe((state) => {
			if (state.isDebugging) {
				console.debug(state);
			}
		})

		if (this.isVisible) {
			this.click()
		}
		
	}

	click() {
		if (this.disabled) { return }
		this.debugToggle.click()
	}

	render() {
		if (this.disabled) { return }
		this.debugToggle.checked = this.isDebugging
		this.element.hidden = !this.isVisible
	}

}