import store from "./store.js";
import latestEvent from "./latestEvent.js";
import MyAudio from "./MyAudio.js";
import MyImage from "./MyImage.js";

export default class Endanzeige {
    constructor() {
		this.yay = new MyAudio("sound/Yay.mp3");
		this.pferd = new MyImage("images/pferd-links.opt.svg")
		this.self = this;
		this.text = "Ende"
		this.visible = false
		this.verloren = false

		store.subscribe((state) => {
			if (latestEvent(state) === 'ENDE') {
				this.visible = true
				this.sterne = state.events.filter(e => e === 'STERN').length
				this.autsch = state.events.filter(e => e === 'AUTSCH').length
				this.verloren = state.verloren

				if (this.verloren) {
					// TODO sound
				}else {
					this.yay.play();
				}
			}
			if (latestEvent(state) === 'START') {
				this.visible = false;
			}
		})
    }

    render(canvas) {
		if (!this.visible) {
			return
		}
		let score = this.sterne;
		let msg = "";
		switch(score) {
			case 0:
				msg = "Du hast leider keine Sterne gesammelt"
				break;
			case 1:
				msg = "Du hast einen Stern gesammelt"
				break;
			default:
				msg = "Du hast " + score + " Sterne gesammelt"
		}

		if (this.autsch == 0) {
			msg += " und keine Fehler gemacht"
		} else {
			msg += " und dich " +this.autsch + " mal verletzt."
		}


		if (this.verloren) {
			msg = "Du hast leider verloren."
		}
		
		let context = canvas.getContext("2d")

		context.fillStyle = "lightgreen";
		context.fillRect(3, 160, canvas.width-6, 200);

		context.fillStyle = "black";
		context.font = "24px Arial";
		context.textAlign = "left";
		context.textBaseline = "top";
		context.fillText("Spiel zuende.", 50 , 200);
		context.fillText(msg, 50, 250);
		context.fillText("Drücke F5 um noch mal zu spielen.", 50, 300);

		this.pferd.x = canvas.width - 300;
		this.pferd.y = canvas.height/2 - 56;
		this.pferd.render(canvas);
    }

}