import latestEvent from "./latestEvent.js";
import MyImage from "./MyImage.js";
import store from "./store.js";
import MyAudio from "./MyAudio.js";
import Hitbox from "./hitbox.js";

function getMousePos(canvas, evt) {
	var rect = canvas.getBoundingClientRect();
	return {
		x: evt.clientX - rect.left,
		y: evt.clientY - rect.top
	};
}
export default class Heldin {
    constructor(maxX, minX=0) {
		this.self = this;
		this.x = 200;
		this.y = 320;
		this.minX = minX;
		this.maxX = maxX;
		this.movingLeft=false;
		this.movingRight=false;
		this.speedPxPerSecond = 256;
		this.jumpStart = 0;
		this.JUMP_DURATION_MS = 1234;

		const slider = document.getElementById('canvas')
		const einDrittelSpielfeldBreite = slider.getBoundingClientRect().width/3

		slider.addEventListener('pointerdown', e2 => {
			let pointerX = getMousePos(slider, e2).x
			
			if (pointerX < einDrittelSpielfeldBreite) { 
				this.movingLeft = true; 
			}
			else if (pointerX > einDrittelSpielfeldBreite * 2) {
				 this.movingRight = true;
			}
			else {
				this.jumpStart = Date.now();
				this.SND_HUEPF.play();
				this.SND_LAUFEN.pause();
			}
		})
		slider.addEventListener('pointerup', e2 => {
			this.movingLeft = false;
			this.movingRight = false;
		})

		this.addArrowKeyListener("keydown", (event) => { 
			if (event.code === 'ArrowLeft') {
				this.movingLeft = true;
			}
			if (event.code === 'ArrowRight') {
				this.movingRight = true;
			}
			if (event.code === 'ArrowUp') {
				this.jumpStart = Date.now();
				this.SND_HUEPF.play();
				this.SND_LAUFEN.pause();
			}
		});
		this.addArrowKeyListener("keyup", (event) => {
			if (event.code === 'ArrowLeft') {
				this.movingLeft = false;
			}
			if (event.code === 'ArrowRight') {
				this.movingRight = false;
			}
		});
		
		this.IMG_PFERD_NORMAL = new MyImage("images/pferd-von-hinten.svg")
		this.IMG_PFERD_JUMP = new MyImage("images/pferd-jump2.svg")
		this.IMG_PFERD_LINKS = new MyImage("images/pferd-links.opt.svg")
		this.IMG_PFERD_RECHTS = new MyImage("images/pferd-links.opt.svg").flipHorizontally()
		this.HITBOX = new Hitbox(this.x, this.y, 55, 80);
		this.HITBOX.setHeightAdjustment(1.5)
		this.maxX -= this.HITBOX.width
		this.minX -= this.HITBOX.width
		this.IMG_SCHMERZEN = new MyImage("images/schmerzsternchen.svg")
		this.SND_AUTSCH = new MyAudio("sound/Boedoelae.mp3");
		this.SND_HUEPF = new MyAudio("sound/Huepfwieher2.mp3");
		this.SND_LAUFEN = new MyAudio("sound/Hufgetrappel.mp3");
		this.SND_LAUFEN.lowVolume()

		this.currentImg = this.IMG_PFERD_NORMAL

		this.isDebugging = store.state.isDebugging;
		this.ende = false
		store.subscribe((state) => {
			this.isDebugging = state.isDebugging;
			if (latestEvent(state) === 'START') {
				this.ende = false;
				this.schmerzEnde = Date.now()-1;
			}
			if (latestEvent(state) === 'AUTSCH') {
				this.SND_AUTSCH.play();
				this.schmerzEnde = Date.now() + state.schmerzDauerMs
			}
			if (latestEvent(state) === 'PREENDE') {
				this.ende = true
			}
		});
	}

	hatSchmerzen() {
		return this.schmerzEnde - Date.now() > 0
	}

	addArrowKeyListener(eventToListenTo, effect) {
		addEventListener(eventToListenTo, (event) => {
			if (event.defaultPrevented) {
				return; // Do nothing if event already handled
			}

			if (event.code.includes("Arrow")) {
				effect(event);

				// Consume the event so it doesn't get handled twice
				event.preventDefault();
			}
		}, false);
	}
    
    render(canvas) {	
		
		if (this.isJumping()) {	
			this.currentImg.drawTo(canvas, this.x, this.y-20);
		} else {
			this.currentImg.drawTo(canvas, this.x, this.y);
		}

		if (this.hatSchmerzen()) {
			this.IMG_SCHMERZEN.drawTo(canvas, this.x, this.y + this.currentImg.height()/4)
		}

		this.HITBOX.render(canvas)
    }

	tick(deltaMillis) {
		if (this.ende) {
			return
		}
		
		let secondsPassed = deltaMillis / 1000;
		let img;
		if (this.isJumping()) {
			img = this.IMG_PFERD_JUMP;
		} else if (this.movingLeft) {
			let newX = Math.max(this.minX, this.x - this.speedPxPerSecond * secondsPassed) 
			this.x = newX;
			img = this.IMG_PFERD_LINKS;
		} else if (this.movingRight) {
			let newX = Math.min(this.maxX, this.x + this.speedPxPerSecond * secondsPassed);
			this.x = newX;
			img = this.IMG_PFERD_RECHTS;
		} else {
			img = this.IMG_PFERD_NORMAL;
		}
		this.currentImg = img;
		
		this.HITBOX.setX(this.x)
		this.HITBOX.setY(this.y)
		this.HITBOX.positionRelativeTo(this.currentImg)
		
		this.schmerzEnde -= deltaMillis

		this.HITBOX.setDisabled(this.isJumping())

		if (!this.isJumping()) {
			this.SND_LAUFEN.playInLoop()
		}
	}

	isJumping(){
		return Date.now() - this.jumpStart < this.JUMP_DURATION_MS
	}

	hitbox() {
		if (this.hatSchmerzen()) {
			return {x:10000000,y:100000,width:0,height:0}
		}

		return {...this.HITBOX.get()}
	}
}