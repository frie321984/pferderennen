function drawFlippedHorizontally(ctx,img,x,y){
    // move to x + img's width
    ctx.translate(x+img.width,y);

    // scaleX by -1; this "trick" flips horizontally
    ctx.scale(-1,1);
    
    // draw the img
    // no need for x,y since we've already translated
    ctx.drawImage(img,0,0);
    
    // always clean up -- reset transformations to default
    ctx.setTransform(1,0,0,1,0,0);
}

export default class MyImage {
    constructor(
		filename
		, callbackOnLoad = ()=>{/*do nothing*/}
		,x=0
		, y=0
	) {
		this.self = this
		let img = new Image();
		this.x = x;
		this.y = y;
		this.isLoaded = false;
		img.onload = () => { 
			this.isLoaded = true 
			callbackOnLoad()
		};
		img.src = filename;

		this.img = img
		this.flipFlag = false;
    }

	flipHorizontally() {
		this.flipFlag = true
		return this.self
	}

	withWidth(w) {
		this.img.width = w
		return this.self
	}

	withScalingBy(percentage=1) {
		if (percentage <0|| percentage>100) {
			throw new Error("percentage must be between 0 and 100")
		}

		this.img.width *= percentage/100
		this.img.height *= percentage/100
		return this.self;
	}

	drawTo(canvas, x=this.x, y=this.y) {
		if (!this.isLoaded) {
			return;
		}
		let context = canvas.getContext("2d");
		
		if (this.flipFlag) {
			drawFlippedHorizontally(context, this.img, x, y)
		} else {
			context.drawImage(this.img, x, y, this.width(), this.height());
		}
	}
	render(canvas) {
		this.drawTo(canvas)
	}

	width() {
		return this.img.width
	}
	height() {
		return this.img.height
	}
}