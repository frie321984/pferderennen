import store from "./store.js";

export default class Hitbox {
    constructor(x=0, y=0, width=0, height=0) {
		this.self = this;
		this.disabled = false;
		this.x = x;
		this.y = y;
		this.width= width;
		this.height= height;
		this.heightAdjustment=2;
		this.widthAdjustment=2;

		this.isDebugging = store.state.isDebugging;
		store.subscribe((state) => {
			this.isDebugging = state.isDebugging;
		});
	}

	positionRelativeTo(img) {
		this.x = this.x + img.width()/this.widthAdjustment - this.width/2;
		this.y = this.y + img.height()/this.heightAdjustment - this.height/2
		return this.self
	}
	setHeightAdjustment(v) {
		this.heightAdjustment = v;
	}
	setWidthAdjustment(v) {
		this.heightAdjustment = v;
	}
	setX(x) {
		this.x = x;
	}
	setY(y) {
		this.y = y;
	}
	setWidth(w) {
		this.width = w;
	}

    render(canvas) {
		if (this.isDebugging && !this.disabled) {
			let ctx = canvas.getContext("2d")
			ctx.beginPath();
			ctx.lineWidth = "0";
			ctx.fillStyle = "red"
			ctx.rect(this.x, this.y, this.width, this.height)
			ctx.fill();
		}
    }

	disable() {
		this.disabled = true;
	}
	enable() {
		this.disabled = false;
	}
	setDisabled(value) {
		this.disabled = value;
	}

	get() {
		if (this.disabled) {
			return {x:1000000,y:1000000,width:0,height:0}
		}
		return {
			x:this.x,
			y:this.y,
			width: this.width,
			height: this.height
		}
	}
	

}