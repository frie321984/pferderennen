import store from "./store.js";
import MyImage from "./MyImage.js";

export default class Anleitung {
    constructor() {
		this.self = this;
		this.visible = true
		this.pferd = new MyImage("images/pferd-links.opt.svg")

		store.subscribe((state) => {
			if (state.events && state.events.length > 0) {
				this.visible = false
			}
		})
    }

    render(canvas) {
		if (!this.visible) {
			return
		}
		
		let context = canvas.getContext("2d")

		context.fillStyle = "lightgreen";
		context.fillRect(3, 60, canvas.width-6, canvas.height-60-120);

		context.fillStyle = "black";
		context.font = "24px Arial";
		context.textAlign = "left";
		context.textBaseline = "top";
		context.fillText("Benutze ⬅ ➡ um dich zu bewegen oder tippe am Rand", 50 , 200);
		context.fillText("Benutze ⬆ um zu hüpfen oder tippe in die Mitte", 50, 250);
		context.fillText("Viel Spaß!  🐎", 50, 300);


		context.fillStyle = "black";
		context.font = "50px Arial Black";
		context.textAlign = "left";
		context.textBaseline = "top";
		context.fillText("PFERDERENNEN", 50 , 80);


		this.pferd.x = canvas.width - 300;
		this.pferd.y = canvas.height/2 - 56;
		this.pferd.render(canvas);
    }

}