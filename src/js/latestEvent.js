export default function latestEvent(state) {
	return state.events && state.events[state.events.length-1]
}