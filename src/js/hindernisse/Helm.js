import MyImage from "../MyImage.js";
import Hindernis from "./hindernis.js";

export default class Reitgerte extends Hindernis {
    constructor(y=-30) {
		super(y)

		this.HITBOX.setHeightAdjustment(2)

		this.img = new MyImage("images/helm.svg", 
			() => { this.HITBOX.setWidth(this.img.width()-20) }
		)
	}
}