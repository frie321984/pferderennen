import MyImage from "../MyImage.js";
import Hitbox from "../hitbox.js";

export default class Hindernis {
    constructor(y=-30) {
		this.self = this;
		this.x = 0;
		this.y = y;

		this.HITBOX = new Hitbox(this.x, this.y, 0, 32);
		this.HITBOX.setHeightAdjustment(5)

		this.img = new MyImage("images/hindernis1.svg", 
			() => { this.HITBOX.setWidth(this.img.width()-20) }
		)
	}

	withRandomX(canvasWidth = 800) {
		let width = this.img.width() || 50;
		console.log(this.img)
		this.x = (Math.random() * (canvasWidth - width)) || 50
		this.HITBOX.setX(this.x);
		this.HITBOX.positionRelativeTo(this.img)
		return this.self
	}

	withX(x=0) {
		this.x = x;
		this.HITBOX.setX(x);
		this.HITBOX.positionRelativeTo(this.img)
		return this.self
	}
	withY(y=0) {
		this.y = y;
		this.HITBOX.setY(y);
		this.HITBOX.positionRelativeTo(this.img);
		return this.self;
	}

    render(canvas) {
		this.img.drawTo(canvas, this.x, this.y);
		
		this.HITBOX.render(canvas)
    }

	tick(deltaMillis) {
		this.y += 3;

		this.HITBOX.setX(this.x);
		this.HITBOX.setY(this.y);
		this.HITBOX.positionRelativeTo(this.img)
	}

	eingesammelt() {
		this.HITBOX.disable();
	}

	remove() {
		this.HITBOX.disable();
	}

	exists() {
		return this.HITBOX && !this.HITBOX.disabled;
	}

	hitbox() {
		return {...this.HITBOX.get()}
	}
	

}