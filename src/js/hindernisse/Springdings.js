import MyImage from "../MyImage.js";
import Hindernis from "./hindernis.js";

export default class Springdings extends Hindernis {
    constructor(y=-30) {
		super(y)

		this.HITBOX.setHeightAdjustment(5)

		this.img = new MyImage("images/hindernis1.svg", 
			() => { this.HITBOX.setWidth(this.img.width()-20) }
		)
	}
}