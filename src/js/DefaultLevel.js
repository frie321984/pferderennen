import store from './store.js';
import Hintergrundmusik from './Hintergrundmusik.js';
import Stern from './stern.js';
import Helm from './hindernisse/Helm.js'
import latestEvent from './latestEvent.js';

export default class DefaultLevel {

	constructor(name, canvas, heldin) {
		this.self = this;
		
		this.name = name;
		this.useHindernisProvider = false;

		this.canvas = canvas;
		this.heldin = heldin;
		this.musik = new Hintergrundmusik();
		this.showLevelTitleDurationMs = 2500;

		// Orte für Hindernisse: LINKS, MITTE, RECHTS
		this.LINKS_PROZENT = 6;
		this.MITTE_PROZENT = 36;
		this.RECHTS_PROZENT = 70;

		this.startTime = Date.now()

		store.subscribe((state) => {
			if (latestEvent(state) === 'LEVEL-START') {
				this.startTime = Date.now()
			}
		})
	}
	
	withHindernisProvider(fun) {
		this.hindernisProvider = fun;
		this.useHindernisProvider = true;
		return this.self;
	}

	tick(delta, remainingTimeMs) {
		if (this.stern && !this.isTitleShowing()) this.stern.tick(delta);
		if (this.hindernis && !this.isTitleShowing()) this.hindernis.tick(delta);
		if (this.heldin) this.heldin.tick(delta);

		if (!this.isTitleShowing()) this.checkHitboxesAndBoundaries(delta)

		if (!this.isTitleShowing() && remainingTimeMs > 3500) {
			this.placeNewStuffWhenNecessary()
		}
	}
	
	placeNewStuffWhenNecessary() {
		if (!this.hindernis || !this.hindernis.exists()) {
			this.neuesHindernisZufaelligPlatziert()
		}
		if (!this.stern || !this.stern.exists()) {
			let sternY = -200
			if (this.hindernis.exists()) {
				sternY = this.hindernis.y - 300; // Abstand halten ;)
			}
			this.stern = new Stern(0, sternY).withRandomX();
		}
	}

	isComplete() {
		return true;
	}

	isHittingBox(source, target) {
		return !(
			( ( source.y + source.height ) < ( target.y ) ) ||
			( source.y > ( target.y + target.height ) ) ||
			( ( source.x + source.width ) < target.x ) ||
			( source.x > ( target.x + target.width ) )
			);
	}

	checkHitboxesAndBoundaries(deltaMillis) {
		if (this.stern) {
			if (this.stern.y > this.canvas.height) {
				this.stern.remove()
			}
			if (this.stern.exists() && this.isHittingBox(this.heldin.hitbox(), this.stern.hitbox())) {
				this.stern.eingesammelt();
				store.dispatch('sternGesammelt');
			}
		}

		if (this.hindernis) {
			if (this.hindernis.y > this.canvas.height) {
				this.hindernis.remove();
			}

			if (this.hindernis.exists() && this.isHittingBox(this.heldin.hitbox(), this.hindernis.hitbox())) {
				this.hindernis.eingesammelt();
				store.dispatch('hindernisGetroffen');
			}
		}
	}	

	neuesHindernis() {
		if (this.useHindernisProvider) {
			return this.hindernisProvider();
		}
		return new Helm(-250)
			.withX(this.canvas.width*m/100);
	}

	neuesHindernisZufaelligPlatziert() {
		let m;
		let r = Math.round(Math.random()*3);
		switch(r) {
			case 2: m=this.LINKS_PROZENT; break;
			case 1: m=this.RECHTS_PROZENT; break;
			case 0: // fallthrough
			default: m=this.MITTE_PROZENT; break;
		}

		this.hindernis = this.neuesHindernis()
		.withY(-250)
		.withX(this.canvas.width*m/100);
	}

	isTitleShowing() {
		return Date.now() - this.startTime < this.showLevelTitleDurationMs;
	}
    
    render() {
		let context = this.canvas.getContext("2d")

		if (this.isTitleShowing()) {
			// Level Title!
			context.fillStyle = "darkgreen";
			context.fillRect(3, this.canvas.height/2-21, canvas.width-6, 80);

			context.fillStyle = "white";
			context.font = "42px Arial";
			context.textAlign = "center";
			context.textBaseline = "center";
			context.fillText(this.name, this.canvas.width / 2, this.canvas.height/2);
		}

		// ------ level in progress ----------

		if (this.stern && !this.isTitleShowing())
			this.stern.render(this.canvas);
		if (this.hindernis && !this.isTitleShowing())
			this.hindernis.render(this.canvas);

		// Hero Layer
		this.heldin.render(this.canvas);

		context.fillStyle = "black";
		context.font = "30px Arial";
		context.textAlign = "right";
		context.textBaseline = "top";
		context.fillText(this.name, this.canvas.width -10 , 10);
	}
}