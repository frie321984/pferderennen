import latestEvent from "./latestEvent.js";
import store from "./store.js";

export default class Gesundheitsanzeige {
    constructor() {
		this.self = this;
		this.leben = 0;

		store.subscribe((state) => {
			if (latestEvent(state) == "ENDE" || latestEvent(state) == "PRE-ENDE") {
				return
			}
			if (!state) {
				return
			}
			
			this.leben = state.uebrigeLeben
		})
    }

	anzeigeText() {
		switch(this.leben) {
			case 3: return "❤️ 🧡 💛"
			case 2: return "❤️ 🧡"
			case 1: return "❤️"
			default: return ""
		}
	}

    render(canvas) {
		let context = canvas.getContext("2d")

		context.fillStyle = "black";
		context.font = "20px Arial";
		context.textAlign = "left";
		context.textBaseline = "top";
		context.fillText(this.anzeigeText(), 10 , 10);
    }

}