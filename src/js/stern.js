import MyImage from "./MyImage.js";
import MyAudio from "./MyAudio.js";
import Hitbox from "./hitbox.js";

export default class Stern {
    constructor(x=0, y=-30) {
		this.self = this;
		this.x = x;
		this.y = y;

		this.img = new MyImage("images/stern.opt.svg")
		this.HITBOX = new Hitbox(this.x, this.y, 20,20);

		this.sound = new MyAudio("sound/Buedueluep.mp3");
	}

	exists() {
		return this.HITBOX && !this.HITBOX.disabled
	}

	withRandomX(canvasWidth = 800) {
		let width = this.img.width() || 50;
		this.x = (Math.random() * (canvasWidth - width)) || 50
		return this.self
	}

    render(canvas) {
		if (this.HITBOX.disabled) {
			return;
		}
		this.img.drawTo(canvas, this.x, this.y);
		this.HITBOX.render(canvas)
    }

	tick(deltaMillis) {
		if (this.HITBOX.disabled) {
			return;
		}
		this.y += 3;

		this.HITBOX.setX(this.x)
		this.HITBOX.setY(this.y)
		this.HITBOX.positionRelativeTo(this.img)
	}

	eingesammelt() {
		this.HITBOX.disable()
		this.sound.play();
	}

	remove() {
		this.HITBOX.disable();
	}

	hitbox() {
		return {...this.HITBOX.get()}
	}
	

}