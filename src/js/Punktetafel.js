import store from "./store.js";
import MyImage from "./MyImage.js";

export default class Punktetafel {
	constructor() {
		this.self = this;
		this.isVisible=false;
		this.sterne = 0;

		store.subscribe((state) => {
			if (!state) {
				return
			}
			this.isVisible = state.isStarted;
			if (state.events) {
				this.sterne = state.events.filter(e => e === 'STERN').length
			}
		})
		this.stern = new MyImage("images/stern.opt.svg").withScalingBy(50)
    }

    render(canvas) {
		if (!this.isVisible) {
			return
		}
		let context = canvas.getContext("2d")

		context.fillStyle = "black";
		context.font = "30px Arial";
		context.textAlign = "right";
		context.textBaseline = "top";
		context.fillText(this.sterne, canvas.width-10, 50);
		
		this.stern.x = canvas.width-90
		this.stern.y = 44;
		this.stern.render(canvas)
	}
}