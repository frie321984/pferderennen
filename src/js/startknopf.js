
import store from './store.js';

export default class Startknopf {
    constructor() {
		this.btn = document.createElement("button")
		document.getElementById("spiel").appendChild(this.btn)

		this.self = this;
		this.gameInProgress = false;

		this.btn.addEventListener("click", event => {
			store.dispatch('start');
		})

		store.subscribe((state) => {
			this.gameInProgress = state.isStarted;
			this.render()
		})
    }

    render() {
		this.btn.innerHTML = 'Los geht\'s!'
		
		this.btn.classList.toggle("hide", this.gameInProgress);
    }

}