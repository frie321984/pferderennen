default: build

build: src
	npm ci
	npm run build
	mv out build

serve: src
	cd src && python3 -m http.server

clean:
	rm -rf target
	rm -rf build

.PHONY: serve
